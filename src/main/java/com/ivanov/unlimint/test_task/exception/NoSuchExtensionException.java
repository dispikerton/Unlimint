package com.ivanov.unlimint.test_task.exception;

public class NoSuchExtensionException extends RuntimeException{
    public NoSuchExtensionException(String message) {
        super(message);
    }
}
